/*
export function someMutation (state) {
}
*/

export function changeValue (state, payload) {
  state.characters[payload.charId][payload.name] = payload.value;
}
