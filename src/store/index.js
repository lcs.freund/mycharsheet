import Vue from 'vue'
import Vuex from 'vuex'

// we first import the module
import characters from './characters'
import VuexPersistence from "vuex-persist";

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    plugins: [new VuexPersistence().plugin],
    state: {
      characters: [

      ]
    },
    modules: {
      // then we reference it
      characters
    },
    mutations: {
      removeChar (state, payload) {
        var charInfo = this.charById(state, payload['id']);
        state.characters.characters.splice(charInfo['index'], 1);
      },
      changeValue (state, payload) {
        var char = state.characters.characters[payload.charId];
        if (payload.subCategory !== undefined) {
          char[payload.subCategory].forEach(element => {
            if (element.name === payload.name) {
              element.value = payload.value;
            }
          });
        } else {
          char[payload.name] = payload.value;
        }
      },

      addSubValue(state, payload) {
        var char = state.characters.characters[payload.charId];
        var name = payload.nProp.name;
        var value = payload.nProp.value;
        var nval = {
          name: name,
          value: value
        }
        char[payload.nProp.subCategory].push(nval);
      },
      addStandardChar(state, payload) {
        let newId = state.characters.characters.length;
        var char = state.characters.characters.find(char => char.id === newId);
        while (char) {
          newId += 1;
          char = state.characters.characters.find(char => char.id === newId);
        }
        state.characters.characters.push({
          id: newId,
          name: "New Character",
          level: 1,
          age: 18,
          gender: 'male',
          class: 'class',
          armor: 1,
          initiative: 1,
          speed: 1,
          hp: {
            current: 10,
            max: 10
          },
          description: "New Character Description",
          attributes: [
            { 'name': 'Stärke', value: 0 },
            { 'name': 'Geschicklichkeit', value: 0 },
            { 'name': 'Konstitution', value: 0 },
            { 'name': 'Intelligenz', value: 0 },
            { 'name': 'Weisheit', value: 0 },
            { 'name': 'Charisma', value: 0 }
          ],
          skills: [
            { 'name': 'Athletik', value: 0 },
            { 'name': 'Akkrobatik', value: 0 },
            { 'name': 'Fingerfertigkeit', value: 0 },
            { 'name': 'Heimlichkeit / Schleichen', value: 0 },
            { 'name': 'Arkane Kunst', value: 0 },
            { 'name': 'Geschichte', value: 0 },
            { 'name': 'Investigation', value: 0 },
            { 'name': 'Naturkunde', value: 0 },
            { 'name': 'Religion', value: 0 },
            { 'name': 'Technik', value: 0 },
            { 'name': 'Wissenschaft', value: 0 },
            { 'name': 'Medizin', value: 0 },
            { 'name': 'Mit Tieren Umgehen', value: 0 },
            { 'name': 'Einsicht', value: 0 },
            { 'name': 'Überlebenskunst', value: 0 },
            { 'name': 'Wahrnehmung', value: 0 },
            { 'name': 'Performance / Aufreten', value: 0 },
            { 'name': 'Einschüchtern', value: 0 },
            { 'name': 'Täuschen / Lügen', value: 0 },
            { 'name': 'Überreden / Überzeugen', value: 0 }
          ],
          inventory: [
            {
              name: 'sword',
              description: 'Ein Schwert',
              stats: [
                { name: 'test', value: 10 }
              ]
            }
          ],
          spells: [
            {
              name: 'fire',
              description: 'Feuerball',
              cantrip: false,
              stats: [
                { name: 'test', value: 10 }
              ]
            },
            {
              name: 'light',
              description: 'Licht',
              cantrip: true,
              stats: [
                { name: 'stat', value: 10 }
              ]
            }
          ]
        });
      },
      overwriteAll(state, payload) {
        state.characters = payload.characters;
      }
    },
    actions: {

    },
    getters: {
      characterById: (state) => (id) => {
        id = Number(id);
        var char = state.characters.characters.find(char => char.id === id);

        return char;
      }
    },
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  Store.charById = (state, pid) => {
    console.log(state);
    console.log(pid);
    var result = {}
    result['index'] = -1;
    result['character'] = state.characters.characters.find((o, i) => {
      result['index'] = i;
      return o.id === pid;
    })
    return result;
  }

  return Store
}
